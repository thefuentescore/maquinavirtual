package tp.pr2.mv.comandos;

import tp.pr2.mv.Constantes;
/**
 * Clase parecida a Step, pero que en vez de ejecutar una sola vez el comando lo hace n veces.
 * @See Step
 * @author Sergio
 *
 */
public class Steps extends Step {
	private int operando;
	
	
	/**
	 * Constructor de la clase, se incializa el atributo operando, que es
	 * las veces que se va ejecutar el comando Step()
	 * @param op
	 */
	public Steps(int op) {
		this.operando = op;
	}

	@Override
	/**
	 * Ejecuta step n veces, donde n = operando. Se sale de la ejecucion si ocurre algun error.
	 */
	public boolean executeCommand() {
		boolean ok = true;
		int i = 0;
		while (i < this.operando && ok && !CommandInterpreter.cpu.abortComputation() )
		{
			ok = super.executeCommand();
			i++;
		}
		
		return ok;
	}

	@Override
	/**
	 * Mtodo que parsea un array de strings y devuelve un nuevo comando Steps o null.
	 * @return new Steps(n)
	 */
	public CommandInterpreter parse(String[] str) {
		if ((str.length == 2) && (str[0].equalsIgnoreCase("STEP") && Constantes.isNumeroPositivo(str[1]) && Constantes.isNumero(str[1])))
			return new Steps(Integer.parseInt(str[1]));
		else return null;
	}

}
