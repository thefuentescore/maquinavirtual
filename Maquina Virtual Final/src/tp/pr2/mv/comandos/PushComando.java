package tp.pr2.mv.comandos;

import tp.pr2.mv.Constantes;

public class PushComando extends CommandInterpreter{
	protected int operando;
	/**
	 * Constructor, inicializa el operando del comando.
	 * @param op
	 */
	public PushComando(int op){
		this.operando = op;
	}
	@Override
	/*
	 * Apila el op en la pila.
	 */
	public boolean executeCommand() {
		CommandInterpreter.cpu.push(operando);
		return true;
	}

	@Override
	public CommandInterpreter parse(String[] str) {
		if ((str.length == 2) && (str[0].equalsIgnoreCase("Push") && Constantes.isNumero(str[1])))
			return new PushComando(Integer.parseInt(str[1]));
		else return null;
	}

}
