package tp.pr2.mv.comandos;


public class PopComando extends CommandInterpreter{

	@Override
	/**
	 * ejecuta un pop del cpu, que elimina la cima de la pila
	 */
	public boolean executeCommand() {
		CommandInterpreter.cpu.pop();
		return true;
	}

	@Override
	/**
	 * parsea un array de string en un nuevo comando Pop
	 */
	public CommandInterpreter parse(String[] str) {
		if ((str.length == 1) && (str[0].equalsIgnoreCase("POP")))
			return new PopComando();
		else
			return null;
	}

}
