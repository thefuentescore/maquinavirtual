package tp.pr2.mv.comandos;

public class Run extends Step {

	@Override
	/*
	 * Ejecuta el comando step hasta que se terminen las instrucciones o haya un error.
	 */
	public boolean executeCommand() {
		boolean ok = true;
		cpu.resetCPU();

		while (!CommandInterpreter.cpu.abortComputation() && ok) {
			ok = super.executeCommand(); // devuelve falso si falla alguna instruccion por ej un add con solo una instruccion
		}
		if (ok) {
			this.isFinished = true;
		}
		return ok;
	}

	@Override
	public CommandInterpreter parse(String[] str) {
		if ((str.length == 1) && (str[0].equalsIgnoreCase("RUN")))
			return new Run();
		else
			return null;
	}

}
