package tp.pr2.mv.comandos;


/**
 * Clase step que implementa los metdos abstractos de la clase @see CommandInterpreter
 * @author Sergio & David
 *
 */
public class Step extends CommandInterpreter {
	
	@Override
	/**
	 * Metodo que ejecuta el comando step, y si se ejecuta correctamente mostrara el estado de la maquina
	 * en caso contrario saldra un mensaje de error
	 * @return booleano
	 */
	public boolean executeCommand() {
		boolean ok = CommandInterpreter.cpu.step();
		if (ok) {
			printStateMachine();
		} 
		else// if (!cpu.isFinishedCorrectly())
			System.out.println("Error en la ejecucion!");

		return ok;
	}

	@Override
	/**
	 * Metodo que parsea un array de strings y devuelve un nuevo comando Step o null.
	 * @return new Step()
	 */
	public CommandInterpreter parse(String[] str) {
		if ((str.length == 1) && (str[0].equalsIgnoreCase("STEP")))
			return new Step();
		else
			return null;

	}

}
