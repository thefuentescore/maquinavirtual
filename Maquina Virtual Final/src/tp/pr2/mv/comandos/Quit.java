package tp.pr2.mv.comandos;

public class Quit extends CommandInterpreter{

	@Override
	/*
	 * Pone el atributo isFinished a true.
	 */
	public boolean executeCommand() {
		return (this.isFinished = true);
	}

	@Override
	public CommandInterpreter parse(String[] str) {
		if ((str.length == 1) && (str[0].equalsIgnoreCase("QUIT")) )
			return new Quit();		
		else return null;
	}

}
