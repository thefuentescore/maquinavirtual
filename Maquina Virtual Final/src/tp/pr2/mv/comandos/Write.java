package tp.pr2.mv.comandos;

import tp.pr2.mv.Constantes;

public class Write extends CommandInterpreter{
	protected int posicion;
	protected int value;
	
	/**
	 * Constructor que asigna posicion=p y value=v
	 * @param p
	 * @param v
	 */
	public Write(int p, int v){
		this.posicion = p;
		this.value = v;
	}
	@Override
	/*
	 * Inserta en la posicion de memoria un valor.
	 */
	public boolean executeCommand() {
		CommandInterpreter.cpu.insertarEnMemoria(this.posicion, this.value);
		return true;
	}

	@Override
	public CommandInterpreter parse(String[] str) {
		if ((str.length == 3) && (str[0].equalsIgnoreCase("WRITE") && Constantes.isNumero(str[1]) && Constantes.isNumero(str[2])))
			return new Write(Integer.parseInt(str[1]), Integer.parseInt(str[2]));
		else return null;
	}

}
