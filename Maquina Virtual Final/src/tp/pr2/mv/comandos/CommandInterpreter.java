
package tp.pr2.mv.comandos;

import tp.pr2.mv.virtualMachine.CPU;

abstract public class CommandInterpreter  {

	protected boolean isFinished;
	protected static CPU cpu;
	
	/**
	 * Metodo que ejecuta un comando, devuelve true si se ejecuta correctamente.
	 * @return boolean.
	 */
	public abstract boolean executeCommand();
	
	/**
	 * Parsea un comando desde un array de strings.
	 * @param array de strings
	 * @return Comando parseado
	 */
	public abstract CommandInterpreter parse(String[] str);
	
	/**
	 * Constructor de la clase, inicializa isFinished a false.
	 */
	public CommandInterpreter ()
	{
		this.isFinished = false;
	}
	
	/**
	 * COnfigua el comando, asignando el cpu del comando al que pasamos por paramtro.
	 * @param cpu
	 */
	public static void configureCommandInterpreter(CPU cpu)
	{
		CommandInterpreter.cpu = cpu;
	}
	
	/**
	 * Metodo que devuelve el atributo isFinished
	 * @return
	 */
	public boolean isFinished()
	{
		return this.isFinished;	
	}
	
	/**
	 * Muestra por pantalla el estado de la maquina.
	 */
	public static void printStateMachine()
	{
		System.out.println(cpu.toString());
	}
}
