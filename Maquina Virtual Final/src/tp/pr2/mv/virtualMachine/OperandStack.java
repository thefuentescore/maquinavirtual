package tp.pr2.mv.virtualMachine;

/**
 * Esta clase es la pila de operandos que se encarga de implementar todas las
 * operaciones que se llevan acabo dentro de la pila, como desapilar, apilar
 * etc...
 * 
 * @author Sergio Fuentes & David Rico
 * 
 */
public class OperandStack {

	private int[] num;
	private int tamanyo;
	private int cima;

	/**
	 * Es el constructor de la clase, inicializa el tamao, el array con
	 * tamao, y la cima como el unico elemento del array.
	 */
	public OperandStack() {
		this.tamanyo = 1;
		this.cima = 0;
		this.num = new int[this.tamanyo]; 
	}

	/**
	 * Inserta en la pila el elemento introducido por parametro.
	 * @param n es el nuevo elemento que se introduce en la pila.
	 */
	public void push(int n) {
		if (this.cima == this.tamanyo) {
			resize();
		}
		this.num[this.cima] = n;
		this.cima++;

	}

	/**
	 * Mtodo que mira si la pila est vacia
	 * @return true si el atributo cima = 0;
	 */
	public boolean estaVacia() {
		return (this.cima == 0) ? true : false;
	}

	
	/**
	 * Devuelve el elemento de la cima y lo quita de la pila.	
	 * @return numCima que es el elemento que esto en la cima
	 */
	public int pop() {
		int numCima = this.num[this.cima - 1];
		this.cima--;

		return numCima;

	}
	
	/**
	 * Devuelve el tamao de la pila 
	 * @return cima
	 */
	public int getSize()
	{
		return this.cima;
	}
	
	/**
	 * Devuelve el elemento que est en la cima
	 * @return int (elemento de la cima)
	 */
	public int getElementoCima(){
		return this.num[this.cima-1];
	}

	
	/**
	 * Mtodo que se encarga de duplicar el tamao de la pila. Para ello utiliza un array auxiliar para conservar
	 * los valores anteriores de la pila
	 */
	
	private void resize(){
		int nuevaCapacidad = this.tamanyo*2;
		int aux[] = new int[nuevaCapacidad];
		
		for (int i=0; i < this.tamanyo; i++){
			aux[i] = this.num[i];
		}
		
		this.tamanyo = nuevaCapacidad;
		this.num = aux;
		
	}

	/**
	 * Mtodo que se encarga de mostrar el estado de la pila.
	 *  @return str. String con el estado.
	 */
	public String toString() {
		String str = " ";

		if (this.estaVacia())
			str = "<vacia>";
		else {
			for (int i = 0; i < this.cima; i++) {
				str = str + num[i] + " ";
			}
		}

		return str;
	}
	
	/**
	 * Mtodo que pone el atributo de cima a cero
	 */
	public void vaciarPila()
	{
		this.cima = 0;
	}
}
