package tp.pr2.mv.virtualMachine;

import java.util.ArrayList;

import tp.pr2.mv.Constantes;
import tp.pr2.mv.instrucciones.Instruction;
/**
 * Clase ProgramMV que tiene el array de instrucciones y los metodo correspondientes
 * para poder tratarlas.
 * @author Sergio Fuentes & David Rico
 *
 */
public class ProgramMV {

	private ArrayList<Instruction> program;

	/**
	 * Constructor de la clase
	 */
	public ProgramMV() {
		this.program = new ArrayList<>();

	}

	/**
	 * Introduce una instruccin en el arrayList de instrucciones
	 * @param instr
	 */
	public void push(Instruction instr) {
		// ArrayList implementa el mtodo para redimensionar
		this.program.add(instr);
	}

	/**
	 * Mtodo que devuelve la instrucin i del arraylist.
	 * 
	 * @param i
	 *            Se asume que i es un indice correcto ya que el control se
	 *            hace desde fuera.
	 * @return Instruction
	 */
	public Instruction get(int i) {
		if (this.program.isEmpty())
			return null;
		else
			return this.program.get(i);
	}

	/**
	 * Mtodo que devuelve el nmero de instrucciones en el array
	 *  
	 * @return nmero de instrucciones.
	 */
	public int getSizeProgram() {
		return this.program.size();
	}

	/**
	 * Devuelve los valores de guarda ProgramMV en un string
	 * @return str
	 */
	public String toSring() {
		String str = "";

		for (int i = 0; i < this.program.size(); i++)
			str = str + i + ": " + this.program.get(i).toString()
					+ Constantes.SALTO_LINEA;

		return str;
	}

}
