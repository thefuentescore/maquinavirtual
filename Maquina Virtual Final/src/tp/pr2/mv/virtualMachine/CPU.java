package tp.pr2.mv.virtualMachine;

import tp.pr2.mv.Constantes;
import tp.pr2.mv.instrucciones.Instruction;

/**
 * Clase compuesta por una una memoria y una pila. Tambin tiene un atributo
 * booleano que toma el valor true en caso de salir de la aplicacin.
 * @author Sergio Fuentes & David Rico
 * 
 */

public class CPU {

	private Memory memory;
	private OperandStack stack;
	private boolean exit;
	private ProgramMV program;
	private int pc;// contador de programa
	private boolean correctPC; // para que el contador de programa est dentro del lmite

	/**
	 * Constructor de la clase
	 */
	public CPU() {
		this.exit = false;
		this.memory = new Memory();
		this.stack = new OperandStack();
		this.program = new ProgramMV();
		this.correctPC = true;
		this.pc = 0;
	}
	/**
	 * Get del tamao de la pila
	 * @return la posicin de cima de la pila
	 */
	public int getSizePila() {
		return stack.getSize();
	}
	/**
	 * Ejecuta la instruccin pop, que devuelve la cima de la pila quitando el elemento de esta.
	 * @return la cima
	 */
	public int pop() {
		return this.stack.pop();
	}
	
	/**
	 * Mtodo que nos devuelve el nmero de instrucciones que tiene almacenadas ProgramMV
	 * 	 * @return int (nmero de instrucciones de la programMV )
	 */
	public int numeroInstrucciones(){
		return this.program.getSizeProgram();
	}
	/**
	 * Ejecuta el push, que mete num en la cima de la pila
	 * @param num
	 */
	public void push(int num) {
		this.stack.push(num);
	}
	/**
	 * Incrementea en 1 el valor del contador del programa. Si este no est dentro del limite permitido,
	 * es decir que pc sea menor que el numero de instrucciones en el programa, correctPc ser false
	 */
	public void increaseProgramCounter() {
		this.pc++;
		if (this.pc >= this.program.getSizeProgram())
			this.correctPC = false;
	}
	
	/**
	 * Inserta el elemento n en la posicion pos de memoria.
	 * @param pos
	 * @param n
	 */
	public void insertarEnMemoria(int pos, int n) {
		this.memory.setValue(pos, n);
	}
	/**
	 * Devuleve el elemento de la posicion posDeMemoria
	 * @param posDeMemoria
	 * @return elemento (int)
	 */
	public int valorDeUnaPosicionDeMemoria(int posDeMemoria) {
		return memory.getValue(posDeMemoria); // si no existe la posicion
												// metemos un cero en la pila
	}
	
	/**
	 * Se inicializa el atributo de la clase progam segun el parametro
	 * @param p (tipo programMV)
	 */
	public void loadProgram(ProgramMV p) {
		this.program = p;
	}
	/**
	 * El atributo booleano exit se pone a true
	 */
	public void exit() {
		this.exit = true;
	}
	
	/**
	 * Set del contador de programa, se le asigna el valor pc.
	 * @param pc
	 */
	public void setContadorPc(int pc) {
		this.pc = pc;
	}

	/**
	 * Mtodo que resetea la CPU, es decir,  se pone el contador de programa a 0,se vacia la pila y la memoria.
	 */
	public void resetCPU() {
		this.pc = 0;
		this.stack.vaciarPila();
		this.memory.vaciarMemoria();
	}

	/**
	 * Devuelve la siguiente instruccin a ejecutar, la que est en contador de
	 * programa.
	 * 
	 * @return si esta bien el contador la instruccion, si esta mal null.
	 */
	public Instruction getCurrentInstruction() {
		if (correctPC)
			return this.program.get(pc);
		else
			return null;
	}
	/**
	 * Devuelve el contador de programa
	 * @return pc
	 */
	public int getContadorPc() {
		return pc;
	}

	/**
	 * Devuelve el valor que almacena el atributo exit 
	 * @return boolean exit
	 */
	public boolean isFinishedCorrectly(){
		return this.exit;
	}

	/**
	 * Metodo para saber si hay que salir de la ejecucin. Mira si correct pc es true y si exit es true.
	 * Si alguno de los dos son true entonces el mtodo devolver true.
	 * 
	 * @return boolean 
	 */
	public boolean abortComputation() {
		if ((!this.correctPC) || (this.exit == true)) // la ejecucion debe
														// detenerse
			return true;
		else
			return false;
	}

	/**
	 * Ejecuta la siguiente instruccin, es decir la que esta en la posicion del array del contador de programa.
	 * @return booleano si se ha ejecutado con xito
	 */
	public boolean step() {

		boolean ok = true;
		/*
		 * Antes de hacer el getCurrentInstruction hay que comprobar que el contador de programa este dentro del
		 * rango de el array de intrucciones, que se han parseado. Por ejemplo si tenemos 4 instrucciones
		 * y hacemos un "rjump 10" el contador+10, deberia dar error.
		 */
		if( this.pc >= this.program.getSizeProgram() || (this.pc < 0)){ // aadimos tambien pc < 0 para cuando hacemos un salto RJUMP y es negativo.
			this.correctPC = false;
			ok = false;
		}
		else if (this.getCurrentInstruction() == null)
			ok = false;
		else {
			System.out.println("####################"
					+ Constantes.SALTO_LINEA
					+ "Comienza la ejecucin de "
					+ this.getCurrentInstruction().toString());
			ok = this.getCurrentInstruction().execute(this);
			
		}
	
		return ok;
	}

	/**
	 * Mtodo que devuelve un string con el estado de la pila y de la memoria.
	 * Para ello utiliza toString de pila y de memoria
	 * 
	 * @return String.
	 */
	public String toString() {
		String str;

		str = "El estado de la maquina tras ejecutar la instruccin es:"
				+ Constantes.SALTO_LINEA + 	"Memoria: " + this.memory.toString()
				+ Constantes.SALTO_LINEA + "Pila de operandos: "
				+ this.stack.toString();

		return str;
	}
}
