package tp.pr2.mv.virtualMachine;
/**
 * Clase que simula una memoria interna de un computador, con una capacidad modificable, y con os
 * metodos propios de una memoria.
 * @author Sergio Fuentes & David Rico
 *
 */
public class Memory {
	
	private Integer[] memory;
	private int capacity = 2;

	/**
	 * Constructor de la clase, inicializa la memoria con todo a null
	 */
	public Memory()
	{
		this.memory = new Integer[this.capacity];
		
		for (int i=0; i<capacity; i++)
			this.memory[i] =  null;
	}
	/**
	 * Metodo que comprueba si la memoria esta vacia.
	 * @return booleano si esta vacia true, si no false
	 */
	public boolean estaVacia(){
		boolean vacia = true;
				
		int i= 0;	
		while ((vacia==true) && (i<capacity)){			
			if(memory[i] != null)
				vacia= false;
			
			i++;		
		}
		
		
		return vacia;
	}
	/**
	 * Mtodo que muestra por pantalla el estado de la memoria
	 * @return string con el estado.
	 */
	public String toString(){
		String str = " ";
		
		if (estaVacia()){
			str = "<vacia>";
		}
		
		for (int i = 0 ; i<this.capacity; i++)
			if (this.memory[i] != null)
				str = str + "[" + i + "]:" + memory[i] + " ";	
		
		return str;
	}
	/**
	 * Mtodo que devuelve el valor almacenado en una posicion de memoria que entra como parametro
	 * @param pos . De memoria en la que se accede
	 * @return el valor de la posicion introducida (int)
	 */
	public int getValue(int pos)
	{
		if (pos > this.capacity) 
			return 0;
		else if (this.memory[pos] == null)
			return 0;
		else 
			return this.memory[pos];
	}
	/**
	 * Mtodo que guarda en la posicion de memoria introducida como parametro, el valor
	 * tambien introducido por parametro
	 * @param pos de memoria
	 * @param n  .Valor a guardar
	 */
	public void setValue(int pos,int n){
		
		if(pos >= this.capacity){
			resize(pos*2);
		}
		
		this.memory[pos] = n;		
		
	}
	
	/**
	 * Mtodo que duplica la capacidad de la memoria dependiendo de la posicin a la que queremos acceder. Es decir, multiplicando pos por 2.
	 * @param pos
	 */
	private void resize(int pos){
		int nuevaCapacidad = pos*2;
		Integer newMemory[] = new Integer[nuevaCapacidad];
		
		for (int i=0; i<this.capacity;i++){
			newMemory[i] = this.memory[i];
		}
		for(int i  = this.capacity; i < nuevaCapacidad; i++){
			newMemory[i] = null;
		}
	
		this.capacity = nuevaCapacidad;
		this.memory = newMemory;
		
	}
	
	/**
	 * Mtodo que pone todas las posiciones de memoria a null
	 */
	public void vaciarMemoria(){
			for (int i=0; i<this.capacity ; i++)
				this.memory[i] = null;
		
	}
	

}
