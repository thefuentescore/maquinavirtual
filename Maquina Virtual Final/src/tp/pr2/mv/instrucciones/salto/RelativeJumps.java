package tp.pr2.mv.instrucciones.salto;

import tp.pr2.mv.instrucciones.Instruction;
import tp.pr2.mv.virtualMachine.CPU;

/**
 * Clase abstracta para los saltos relativos, que en vez de saltar a la posicion n del array de instrucciones
 * suman n. Es decir si n = 10, con un JUMP 4--> n=4, pero con un RJUMP 4--> n = 14
 * @author Sergio Fuentes & David Rico 
 *
 */
public abstract class RelativeJumps extends Instruction{
	
	// Atributo protegido que representa el valor de la posicin a la que hay que saltar.
	protected int numSalto;	
	
	/**
	 * Mtodo diseado para las intrucciones de salto relativo condionales RBT y RBF, porque segn el parmetro que le entra van a devolver falso o true
	 * dependiendo en la instruccin que nos encontremos.
	 * @param n (cima de la pila)
	 * @return boolean 
	 */	
	protected abstract boolean execute(int n);

	/**
	 * Devuelve falso en dos casos : 
	 * 				1) Si la pila esta vaca
	 * 				2) Si al proceder el salto se accede a una posicin mayor al nmero de instrucciones o si es una posicin negativa.
	 * 	Devuelve true en dos casos:
	 * 				1) Si se ha realizado un salto condicional sin acceder a una posicin no permita
	 *				2) Si simplemente no se realiza el salto porque no se cumple la condicin correspondiente a BT  BF y en este caso
	 *					se incrementar el contador de programa para pasar a la siguiente instruccin.
	 */	
	@Override
	public boolean execute(CPU cpu) {
		boolean ok = true;
		if (cpu.getSizePila() > 0) {
			int n = cpu.pop();
			if (this.execute(n)) { // se salta pero hay que mirar a donde 
				if ((this.numSalto + cpu.getContadorPc() >=0 ) && (this.numSalto + cpu.getContadorPc() < cpu.numeroInstrucciones()))
				cpu.setContadorPc(cpu.getContadorPc()+ this.numSalto); // se suma al contador de pc actual el numsalto
				else {
					ok = false; // no se puede saltar a un numero negativo
					cpu.exit();
				}
			}
			else // incrementamos porque no se realiza el salto 
				cpu.increaseProgramCounter();
		} else
			ok = false;
		
		
		return ok;
	}
}
