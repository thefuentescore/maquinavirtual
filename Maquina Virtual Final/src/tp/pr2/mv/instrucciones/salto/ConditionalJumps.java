package tp.pr2.mv.instrucciones.salto;

import tp.pr2.mv.instrucciones.Instruction;
import tp.pr2.mv.virtualMachine.CPU;

/**
 * Clase abstracta que hereda de Instrucction la cual se encarga de ejecutar
 * instrucciones de salto.
 * 
 * @author David Rico & Sergio Fuentes
 * 
 */
public abstract class ConditionalJumps extends Instruction {
	
	// Atributo protegido que representa el valor de la posici  n a la que hay que saltar.
	protected int numSalto;
	
	/**
	 * M  todo dise  ado para las intrucciones de salto condionales BT y BF, porque seg  n el par  metro que le entra van a devolver falso o true
	 * dependiendo en la instrucci  n que nos encontremos.
	 * @param n (cima de la pila)
	 * @return boolean 
	 */				
	protected abstract boolean execute(int n);

	/**
	 * Devuelve falso en dos casos : 
	 * 				1) Si la pila esta vac  a
	 * 				2) Si al proceder el salto se accede a una posici  n mayor al n  mero de instrucciones o si es una posici  n negativa.
	 * 	Devuelve true en dos casos:
	 * 				1) Si se ha realizado un salto condicional sin acceder a una posici  n no permita
	 *				2) Si simplemente no se realiza el salto porque no se cumple la condici  n correspondiente a BT    BF y en este caso
	 *					se incrementar   el contador de programa para pasar a la siguiente instrucci  n.
	 */	
	@Override
	public boolean execute(CPU cpu) {
		boolean ok = true;
		if (cpu.getSizePila() > 0) {
			int n = cpu.pop();
			if (this.execute(n)) {
				cpu.setContadorPc(this.numSalto);
				/*Si despues de cambiar el contador este se excede devolvemos false y salimos*/
				if (cpu.getContadorPc() >= cpu.numeroInstrucciones() || (cpu.getContadorPc() < 0)) { 
					ok = false;
					cpu.exit();
				}		
			} else
				// incrementamos porque NO se realiza el salto
				cpu.increaseProgramCounter();
		} else
			ok = false;

		return ok;
	}
}
