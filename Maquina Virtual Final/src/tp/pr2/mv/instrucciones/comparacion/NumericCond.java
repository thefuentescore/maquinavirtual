package tp.pr2.mv.instrucciones.comparacion;

import tp.pr2.mv.instrucciones.Instruction;
import tp.pr2.mv.virtualMachine.CPU;

public abstract class NumericCond extends Instruction {
	/**
	 * Metodo abstracto que compara la cima con la subcima. Devuelve booleano dependiendo de la comparacion.
	 * @param cima
	 * @param subcima
	 * @return booleano
	 */
	protected abstract boolean compare (int cima, int subcima);
	
	@Override
	/*
	 * Coge la cima y la subcima y ejecuta el Compare, si se cumple apila 1, si no se apila 0.
	 */
	public boolean execute(CPU cpu) {
		if (cpu.getSizePila() >= 2)
		{
			int cima = cpu.pop();
			int subcima = cpu.pop();
			if (compare (cima,subcima)) 
				cpu.push(1);
			else
				cpu.push(0);
			
			cpu.increaseProgramCounter();
			return true;
		}
		
		else return false;			
	}

	
	

}
