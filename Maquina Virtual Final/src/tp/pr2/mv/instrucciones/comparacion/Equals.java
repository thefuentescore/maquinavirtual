package tp.pr2.mv.instrucciones.comparacion;

import tp.pr2.mv.instrucciones.Instruction;

public class Equals extends NumericCond {

	@Override
	/*
	 * Si la cima y la subcima son iguales devuelve true.
	 */
	protected boolean compare(int cima, int subcima) {		
		return cima == subcima;
	}

	@Override
	public Instruction parse(String[] str) {
		if ((str.length == 1) && (str[0].equalsIgnoreCase("EQ")))			
			return new Equals();
		
		else return null;
	}

	@Override
	public String toString() {
		
		return "EQUALS" ;
	}

}
