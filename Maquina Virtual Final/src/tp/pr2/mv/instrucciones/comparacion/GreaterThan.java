package tp.pr2.mv.instrucciones.comparacion;

import tp.pr2.mv.instrucciones.Instruction;

public class GreaterThan extends NumericCond {

	@Override
	/*
	 * Si la subcima es mayor que la cima devuelve true.
	 */
	protected boolean compare(int cima, int subcima) {		
		return subcima > cima;
	}

	@Override
	public Instruction parse(String[] str) {
		if ((str.length == 1) && (str[0].equalsIgnoreCase("GT")))			
			return new GreaterThan();
		
		else return null;
	}

	@Override
	public String toString() {
		
		return "GT" ;
	}

}