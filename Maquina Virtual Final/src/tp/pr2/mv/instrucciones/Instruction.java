package tp.pr2.mv.instrucciones;

import tp.pr2.mv.virtualMachine.CPU;


/**
 * Clase abstracta con los metodos execute y parse, de esta heredaran todas las demas instrucciones.
 * @author David Rico & Sergio Fuentes
 *
 */
public abstract class Instruction
{	
	/**	
	 * metodo que ejecuta una instruccion
	 * @param cpu
	 * @return booleano si se ha ejecutado con exito
	 */
	abstract public boolean execute (CPU cpu);
	/**
	 * Parsea un string y devuelve una instruccion si se consigue parsearla bien, si no null.
	 * @param string a parsear
	 * @return instruccion parseada
	 */
	abstract  public Instruction parse (String[] str);

	
}







