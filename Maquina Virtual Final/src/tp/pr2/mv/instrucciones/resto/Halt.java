package tp.pr2.mv.instrucciones.resto;

import tp.pr2.mv.instrucciones.Instruction;
import tp.pr2.mv.virtualMachine.CPU;
/**
 * clase que implementa los metodos execute, parse y tostring, Hace exit para salir del programa.
 * @author David & Sergio
 *
 */
public class Halt extends RestSeq {

		
	@Override	
	/*
	 * Ejecuta el exit
	 */
	protected boolean executeAux(CPU cpu) {
		cpu.exit();
		return true;
	}

	@Override
	public Instruction parse(String[] str) {
		if ( (str.length == 1 ) && (str[0].equalsIgnoreCase("HALT")) ){
			return new Halt();
		}
					
		else return null;
	}

	@Override
	public String toString() {
		return "HALT";
	}

}

