package tp.pr2.mv.instrucciones.resto;

import tp.pr2.mv.instrucciones.Instruction;

import tp.pr2.mv.virtualMachine.CPU;
/**
 * clase que implementa los metodos execute, parse y tostring, Quita la cima de la pila.
 * @author David & Sergio
 *
 */
public class Pop extends RestSeq {

	@Override
	protected boolean executeAux(CPU cpu) {
		cpu.pop();
		return true;
	}

	@Override
	public Instruction parse(String[] str) {
		if ((str.length == 1) && (str[0].equalsIgnoreCase("POP")) )
			return new Pop();		
		else return null;
	}

	@Override
	public String toString() {
		return "POP";
	}

}
