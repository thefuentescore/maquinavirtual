package tp.pr2.mv.instrucciones.resto;

import tp.pr2.mv.instrucciones.Instruction;
import tp.pr2.mv.virtualMachine.CPU;

/**
 * clase que implementa los metodos execute, parse y tostring. Consigue duplicar la cima de la pila.
 * @author David & Sergio
 *
 */
public class Dup extends RestSeq {

		
	@Override
	/*
	 * Duplica la cima de la pila.
	 */
	protected boolean executeAux(CPU cpu) {
		if (cpu.getSizePila() < 1)
			return false;
		else
		{
			int n = cpu.pop();
			cpu.push(n);
			cpu.push(n);
			return true;
		}
		
	}
	
	@Override
	public Instruction parse(String[] str) {
		if ( (str.length == 1 ) && (str[0].equalsIgnoreCase("DUP")) ){
			return new Dup();
		}
					
		else return null;
	}

	@Override
	public String toString() {
		return "DUP";
	}
	
}
 

