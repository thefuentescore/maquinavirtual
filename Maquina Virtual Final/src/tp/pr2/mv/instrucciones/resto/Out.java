package tp.pr2.mv.instrucciones.resto;

import tp.pr2.mv.instrucciones.Instruction;
import tp.pr2.mv.virtualMachine.CPU;
/**
 * clase que implementa los metodos execute, parse y tostring, Muestra por pantalla el caracter asociado al numero 
 * de la cima.
 * @author David & Sergio
 *
 */
public class Out extends RestSeq {

		
	@Override
	/*
	 * Muestra por pantalla el valor del caracter de la cima de la pila.
	 */
	protected boolean executeAux(CPU cpu) {
		if (cpu.getSizePila() < 1)
			return false;
		else
		{
			System.out.println((char) cpu.pop());
			return true;
		}
		
	}

	@Override
	public Instruction parse(String[] str) {
		if ( (str.length == 1 ) && (str[0].equalsIgnoreCase("OUT")) ){
			return new Out();
		}
					
		else return null;
	}

	@Override
	public String toString() {
		return "OUT";
	}
	
}
