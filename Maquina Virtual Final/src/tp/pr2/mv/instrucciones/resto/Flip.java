package tp.pr2.mv.instrucciones.resto;

import tp.pr2.mv.instrucciones.Instruction;
import tp.pr2.mv.virtualMachine.CPU;
/**
 * clase que implementa los metodos execute, parse y tostring. Consigue intercambiar la cima y la subcima de la pila.
 * @author David & Sergio
 *
 */
public class Flip extends RestSeq {

		
	@Override
	/*
	 * Intercambia el valor de la cima con el de la subcima.
	 */
	protected boolean executeAux(CPU cpu) {
		if (cpu.getSizePila() < 2)
			return false;
		else 
		{
			int n1 = cpu.pop();
			int n2 = cpu.pop();
			cpu.push(n1);
			cpu.push(n2);
			return true;
		}
		
	}

	@Override
	public Instruction parse(String[] str) {
		if ( (str.length == 1 ) && (str[0].equalsIgnoreCase("FLIP")) ){
			return new Flip();
		}
					
		else return null;
	}

	@Override
	public String toString() {
		return "FLIP";
	}

}

