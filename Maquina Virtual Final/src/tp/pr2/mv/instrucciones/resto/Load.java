package tp.pr2.mv.instrucciones.resto;

import tp.pr2.mv.Constantes;
import tp.pr2.mv.instrucciones.Instruction;
import tp.pr2.mv.virtualMachine.CPU;
/**
 * clase que implementa los metodos execute, parse y tostring, Extrae un valor de una posicion de memoria y la apila.
 * @author David & Sergio
 *
 */
public class Load extends RestSeq {

	private int posMemoria;
	/**
	 * Constructor.
	 * @param n, posicion de memoria de la qe
	 */
	public Load(int n){
		this.posMemoria = n;
	}
	
	@Override
	/*
	 * Inserta en una posicion de memoria de la cpu un valor.
	 */
	protected boolean executeAux(CPU cpu) {
		int valorApila = cpu.valorDeUnaPosicionDeMemoria(this.posMemoria);
		cpu.push(valorApila); // si la posicion no existe apilamos un cero
		return true;		
	}

	
	@Override
	public Instruction parse(String[] str) {
		// Se parsea que se salte a un nmero y ademas que no sea un nmero negativo ya que no se podra acceder.
		if ( (str.length == 2 ) && (str[0].equalsIgnoreCase("LOAD"))  && Constantes.isNumeroPositivo(str[1]) && Constantes.isNumero(str[1]) ){
			int n = Integer.parseInt(str[1]);
			return new Load(n);
		}
					
		else return null;
	}

	@Override
	public String toString() {
		return "LOAD " + this.posMemoria;
	}



}
