package tp.pr2.mv.instrucciones.resto;

import tp.pr2.mv.instrucciones.Instruction;
import tp.pr2.mv.virtualMachine.CPU;
/**
 * Clase abstracta con metodo executeAux, Implementa el execute de la clase instruction
 * 
 * @author David & Sergio
 *
 */
public abstract class RestSeq extends Instruction{
	/**
	 * Es un ejecuta auxiliar que devuelve true si se ejecuta sin errores, se utilza dentro del Ejecuta principal.
	 * @param cpu
	 * @return boolean, true si se hace correctamente y false si no.
	 */
	abstract protected boolean executeAux(CPU cpu);
	
	@Override
	public boolean execute(CPU cpu) {
		if(this.executeAux(cpu)){
			cpu.increaseProgramCounter();
			return true;
		}
		else return false;
	}
}
