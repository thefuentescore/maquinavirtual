package tp.pr2.mv.main;

import java.util.Scanner;

import tp.pr2.mv.comandos.CommandInterpreter;
import tp.pr2.mv.instrucciones.Instruction;
import tp.pr2.mv.virtualMachine.CPU;
import tp.pr2.mv.virtualMachine.ProgramMV;

/**
 * Clase principal encargada de arrancar la aplicacion
 * @author Sergio Fuentes & David Rico
 */
public class Main {
	/**
	 * Metodo estatico que se encarga de leer el programa fuente parseando
	 * cada una de las instrucciones que se vayan metiendo.
	 * @return
	 */
	static private ProgramMV readProgram() {

		ProgramMV programMv = new ProgramMV();
		Instruction ins = null;
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String linea = new String();
		
		linea = sc.nextLine();

		while (!linea.equalsIgnoreCase("end")) {
			ins = InstructionParser.parseoUnaPosibleIntruccion(linea);
			if (ins != null)
				programMv.push(ins);
			else
				System.out.println("Instruccion incorrecta");
			linea = sc.nextLine();
		}

		// sc.close();
		return programMv;

	}

	/**
	 * Metodo main, se ocupa de ejecutar el programa. 
	 * @param args
	 */

	public static void main(String[] args) {

		CPU cpu = new CPU();
		CommandInterpreter.configureCommandInterpreter(cpu);
		System.out.println("Introduce el programa fuente: ");
		System.out.print("> ");
		ProgramMV programMv = readProgram();// leemos todas las instrucciones
		System.out.println(programMv.toSring());

		cpu.loadProgram(programMv);
		CommandInterpreter comando = null;

		Scanner sc = new Scanner(System.in); 
		String str;
		do {
			System.out.print("> ");
			str = sc.nextLine(); // cogemos la siguiente linea de la consola
			comando = CommandParser.parsePosibleCommand(str);// parseamos
			
			while(comando == null){ //while para cada vez que lo lea mal te pida de nuevo
				System.out.println("No te entiendo!");
				System.out.print("> ");
				str = sc.nextLine();
				comando = CommandParser.parsePosibleCommand(str);
			}
			comando.executeCommand();
			
		} while (!comando.isFinished() && !cpu.abortComputation());
		

		sc.close(); // se cierra el lector

	}
	

}
