package tp.pr2.mv.main;

import tp.pr2.mv.Constantes;
import tp.pr2.mv.comandos.CommandInterpreter;
import tp.pr2.mv.comandos.PopComando;
import tp.pr2.mv.comandos.PushComando;
import tp.pr2.mv.comandos.Quit;
import tp.pr2.mv.comandos.Run;
import tp.pr2.mv.comandos.Steps;
import tp.pr2.mv.comandos.Step;
import tp.pr2.mv.comandos.Write;

/**
 * Clase que se encarga de parsear strings a comandos
 * @author David Rico & Sergio Fuentes
 *
 */
public class CommandParser {
	
	/**
	 * Array esttico de tipo Commando
	 */
	private static CommandInterpreter commandSet[] = 
		{
			new Quit(), new Run(), new Step(), new Steps(0), new PushComando(0), new PopComando(), new Write(0, 0)
		};
	

	/**
	 * Mtodo esttico que parsea un string en una instruccion
	 * @param posibleCommand
	 * @return Comando parseado si existe y si no devolver null.
	 */
	public static CommandInterpreter parsePosibleCommand(String  posibleCommand) //ej: push 5 
	{
		int i = 0;
		boolean stop = false;
		String[] comando_posible = Constantes.separarString(posibleCommand);
		CommandInterpreter comand = null;
		
		while (i<CommandParser.commandSet.length && !stop)
		{
			comand = CommandParser.commandSet[i].parse(comando_posible);
			if (comand != null) 
				stop = true;
			else 
				i++;
		}
		
		return comand;
	}
	

	

}
