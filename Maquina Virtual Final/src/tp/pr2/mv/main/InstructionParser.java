package tp.pr2.mv.main;


import tp.pr2.mv.Constantes;
import tp.pr2.mv.instrucciones.Instruction;
import tp.pr2.mv.instrucciones.aritmeticas.Add;
import tp.pr2.mv.instrucciones.aritmeticas.Div;
import tp.pr2.mv.instrucciones.aritmeticas.Mul;
import tp.pr2.mv.instrucciones.aritmeticas.Sub;
import tp.pr2.mv.instrucciones.booleanas.And;
import tp.pr2.mv.instrucciones.booleanas.Not;
import tp.pr2.mv.instrucciones.booleanas.Or;
import tp.pr2.mv.instrucciones.comparacion.Equals;
import tp.pr2.mv.instrucciones.comparacion.GreaterThan;
import tp.pr2.mv.instrucciones.comparacion.LessOrEqual;
import tp.pr2.mv.instrucciones.comparacion.LessThan;
import tp.pr2.mv.instrucciones.resto.Dup;
import tp.pr2.mv.instrucciones.resto.Flip;
import tp.pr2.mv.instrucciones.resto.Halt;
import tp.pr2.mv.instrucciones.resto.Load;
import tp.pr2.mv.instrucciones.resto.Out;
import tp.pr2.mv.instrucciones.resto.Pop;
import tp.pr2.mv.instrucciones.resto.Push;
import tp.pr2.mv.instrucciones.resto.Store;
import tp.pr2.mv.instrucciones.salto.Bf;
import tp.pr2.mv.instrucciones.salto.Bt;
import tp.pr2.mv.instrucciones.salto.Jump;
import tp.pr2.mv.instrucciones.salto.RJump;
import tp.pr2.mv.instrucciones.salto.Rbf;
import tp.pr2.mv.instrucciones.salto.Rbt;


/**
 * Clase que se encarga de parsear strings a instrucciones
 * @author Sergio Fuentes & David Rico
 *
 */
public class InstructionParser {
	
	
	/**
	 * Array de conjunto de instrucciones
	 */
	private static Instruction instructionSet[] = 
		{
		new Add(), new Sub(), new Mul(), new Div(),
		new And(), new Not(), new Or(),
		new Equals(), new GreaterThan(), new LessOrEqual(), new LessThan(),
		new Dup(), new Flip(), new Halt(), new Load(0), new Out(), new Pop(), new Push(0), 
		new Store(0), new Bf(0), new Bt(0), new Jump(0), new RJump(0), new Rbf(0), new Rbt(0)
		
		};
	

	/**
	 * Mtodo esttico que parsea un string en una instruccion
	 * @param string a parsear
	 * @return Instruccion parseada si existe y si no devolver null.
	 */
	public static Instruction parseoUnaPosibleIntruccion(String  posibleInstruccion) //ej: push 5 
	{
		int i = 0;
		boolean stop = false;
		String[] posibleInstr = Constantes.separarString(posibleInstruccion);
		Instruction ins = null;
		
		while (i<InstructionParser.instructionSet.length && !stop)
		{
			ins = InstructionParser.instructionSet[i].parse(posibleInstr);
			if (ins != null) 
				stop = true;
			else 
				i++;
		}
		
		return ins;
	}
	
	
}



